import argparse


def process(data_file, some_value):
    with open(data_file, 'r') as fr:
        contents = fr.read()
        with open('pge_outputs.txt', 'w') as fw:
            fw.write(contents)
            fw.write("\n")
            fw.write(f"This value provided {some_value}")


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("--data_file", required=True)
    parser.add_argument("--some_value", required=True)
    args = parser.parse_args()
    process(args.data_file, args.some_value)
